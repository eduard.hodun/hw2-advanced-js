const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


const div = document.getElementById("root");
const ulList = document.createElement("ul");

div.appendChild(ulList);

books.forEach(book => {
    try {
        if (!book.author || !book.name || !book.price) {
            const noSomeKeys = [];

            if (!book.author) {
                noSomeKeys.push('author');
                 };

            if (!book.name) {
                noSomeKeys.push('name');

            };

            if (!book.price) {
                noSomeKeys.push('price');

            };

            throw new Error(`У книзі відсутня наступна властивість - ${noSomeKeys}`);
        }

        const li = document.createElement("li");
        const bookReview = `Назва книгі: ${book.name} ------- >>>  Автор книгі: ${book.author} ----->>>   Ціна книгі: ${book.price}`;
        li.textContent = bookReview;
        ulList.appendChild(li);
    } catch (error) {
        console.error(error.message);
    }
});

1.Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

Коли при виконанні програми виникає помилка, то створюється об'єкт з її описом, який перехоплюється та обробляється. 
У звичайних ситуаціях, коли виникає виняток, то програма припиняє своє виконання і в консоль, 
в середовище розробки чи у файл виводяться відомості про помилку. У мові JavaScript є можливість обробки винятків, 
коли при виникненні помилки виконання програма не завершується, а здійснює певні операції, вказані програмістом.

JavaScript має вбудований метод JSON.parse(str), який використовується для читання JavaScript-об'єктів (і не тільки)з рядка.

Зазвичай він використовується для обробки даних, отриманих по мережі, з сервера або з іншого джерела.

Якщо дані некоректні, JSON.parse генерує помилку, тобто скрипт «впаде».

Виходить, якщо раптом щось не так з даними, то відвідувач ніколи (якщо, звичайно, не відкриє консоль) про це не дізнається.

Саме в таких і подібних випадках потрібна конструкція try...catch.